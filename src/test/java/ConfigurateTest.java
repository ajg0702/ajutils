import org.junit.Test;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.yaml.NodeStyle;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.*;

public class ConfigurateTest {
    @Test
    public void TestSubNodes() throws IOException {
        File file = new File("temp.yml");
        System.out.println(file.getAbsolutePath());
        file.delete();
        YamlConfigurationLoader loader = YamlConfigurationLoader.builder().file(file).nodeStyle(NodeStyle.BLOCK).build();
        CommentedConfigurationNode config =
                loader.load();

        config.node("one").node("two").node("three").set("four");

        loader.save(config);

        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        file.delete();
    }
}
