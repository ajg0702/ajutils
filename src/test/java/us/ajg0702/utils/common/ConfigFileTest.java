package us.ajg0702.utils.common;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

public class ConfigFileTest {
    @Test
    public void TestConfigConverter() throws Exception {
        File base = new File("./build/tmp/test");
        try (
                InputStream is = this.getClass().getResourceAsStream("/test-existing.yml");
                OutputStream os = new FileOutputStream(new File(base, "test.yml"))
                ) {
            byte[] buffer = new byte[1024];
            int length;
            assert is != null;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }




        ConfigFile config = new ConfigFile(
                base,
                Logger.getGlobal(),
                "test.yml");

        if(config.getString("value").equalsIgnoreCase("default")) {
            throw new Exception("Default value found! Config converter bad!");
        }

        if(!config.getString("value").equalsIgnoreCase("existing")) {
            throw new Exception("Not existing value found! Config converter bad!");
        }

        if(!config.getStringList("list-test").contains("one")) {
            throw new Exception("List is invalid!");
        }



    }

    @Test
    public void TestRealConfigConverter() throws Exception {
        File realBase = new File("./build/tmp/test");
        try (
                InputStream is = this.getClass().getResourceAsStream("/real-existing.yml");
                OutputStream os = new FileOutputStream(new File(realBase, "real.yml"))
        ) {
            byte[] buffer = new byte[1024];
            int length;
            assert is != null;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }

        ConfigFile real = new ConfigFile(
                realBase,
                Logger.getLogger(Logger.GLOBAL_LOGGER_NAME),
                "real.yml"
        );

        if(!real.getString("wait-time").equals("6")) {
            throw new Exception("Default value found in real! Config converter bad (wait-time)");
        }
        if(!real.getStringList("balancer-types").contains("testing this")) {
            throw new Exception("List is invalid! (balancer-types)");
        }
    }

}