package us.ajg0702.utils.common;

import junit.framework.TestCase;
import org.junit.Test;

public class MessagesTest extends TestCase {

    @Test
    public void testMakeDefaults() throws Exception {
        System.out.println(Messages.makeDefaults("a", "b", "c", "d"));

        try {
            Messages.makeDefaults("a", "b", "c");
            throw new Exception("Odd arg length didnt throw error!");
        } catch(IllegalArgumentException ignored) {}
    }
}