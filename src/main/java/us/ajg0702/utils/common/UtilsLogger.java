package us.ajg0702.utils.common;

public interface UtilsLogger {
    void warn(String message);
    void warning(String message);
    void info(String message);
    void error(String message);
    void severe(String message);
}
