package us.ajg0702.utils.common;

import java.util.List;
import java.util.Random;

@SuppressWarnings("unused")
public class GenUtils {
	public static int randomInt(int min, int max) {
		if (min > max) {
			int temp = min;
			min = max;
			max = temp;
		} else if(min == max) {
			return min;
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	public static boolean stringContains(List<String> list, String string) {
		for(String ss : list) {
			if(ss.equalsIgnoreCase(string)) {
				return true;
			}
		}
		return false;
	}
}
