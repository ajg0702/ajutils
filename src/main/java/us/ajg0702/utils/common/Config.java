package us.ajg0702.utils.common;

import org.bukkit.plugin.java.JavaPlugin;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class Config implements ConfigInterface {

	ConfigFile cf;

	public Config(File dataFolder, Logger logger) throws ConfigurateException {
		this(dataFolder, logger, null);
	}
	public Config(File dataFolder, Logger logger, Class<?> getDefaultFrom) throws ConfigurateException {
		cf = new ConfigFile(dataFolder, logger, "config.yml", getDefaultFrom);
	}

	public Object get(String key) {
		return cf.get(key);
	}
	public Integer getInt(String key) {
		return cf.getInt(key);
	}
	public String getString(String key) {
		return cf.getString(key);
	}
	public List<String> getStringList(String key) {
		return cf.getStringList(key);
	}
	public boolean getBoolean(String key) {
		return cf.getBoolean(key);
	}
	public double getDouble(String key) {
		return cf.getDouble(key);
	}

	public ConfigFile getConfigFile() {
		return cf;
	}

	public CommentedConfigurationNode getNode() {
		return cf.getConfig();
	}

	public boolean hasEntry(String key) {
		return cf.hasEntry(key);
	}
	
	public void reload() throws ConfigurateException {
		cf.reload();
	}
}
