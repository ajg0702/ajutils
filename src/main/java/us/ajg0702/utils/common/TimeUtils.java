package us.ajg0702.utils.common;

@SuppressWarnings("unused")
public class TimeUtils {
    public static String timeString(int time, String minuteFormat, String secondFormat) {
        int min = time / 60;
        int sec = time % 60;
        if(min > 0) {
            return minuteFormat
                    .replaceAll("\\{m}", min+"")
                    .replaceAll("\\{s}", sec+"");
        }
        return secondFormat
                .replaceAll("\\{m}", "0")
                .replaceAll("\\{s}", sec+"");
    }
    public static String timeString(int time) {
        return timeString(time, "{m}m {s}s", "{s}s");
    }
}

