package us.ajg0702.utils.common;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Older class. For new implementations, use UpdateManager and the plugin-updater stuff
 */
@SuppressWarnings("unused")
public class Updater {

    private final ScheduledExecutorService updateExecutor = Executors.newScheduledThreadPool(1);

    private final UtilsLogger logger;
    private final String currentVersion;
    private final String jarName;
    private boolean enabled;
    private final int resrouceId;
    private final File pluginsFolder;
    private final String downloadCommand;

    private String latestVersion;

    boolean updateAvailable = false;

    boolean ready = false;

    boolean alreadyDownloaded = false;

    public Updater(UtilsLogger logger, String currentVersion, String jarName, boolean enabled, int resourceId, File pluginsFolder, String downloadCommand) {
        this.logger = logger;
        this.jarName = jarName;
        this.enabled = enabled;
        this.resrouceId = resourceId;
        this.pluginsFolder = pluginsFolder;
        this.downloadCommand = downloadCommand;

        if(currentVersion.contains("-")) {
            this.currentVersion = currentVersion.split("-")[0];
        } else {
            this.currentVersion = currentVersion;
        }

        updateExecutor.scheduleAtFixedRate(() -> {
            try {
                check();
            } catch(Exception e) {
                logger.info("Unable to check for update: "+e.getMessage());
            }
        }, 2, 120, TimeUnit.SECONDS);
    }

    public void check() throws IOException {
        if(!enabled) return;

        URL url = new URL("https://api.spigotmc.org/simple/0.2/index.php?action=getResource&id="+resrouceId);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("User-Agent", jarName+"/"+currentVersion);

        if(connection.getResponseCode() != 200) {
            throw new IllegalStateException("Response code was "+connection.getResponseCode());
        }

        InputStream inputStream = connection.getInputStream();
        InputStreamReader reader = new InputStreamReader(inputStream);

        JsonObject json = new JsonParser().parse(reader).getAsJsonObject();

        latestVersion = json.get("current_version").getAsString();

        if(latestVersion.contains("-")) {
            latestVersion = latestVersion.split("-")[0];
        }

        if(latestVersion.isEmpty()) {
            throw new IllegalStateException("Latest version is empty!");
        }

        String[] parts = latestVersion.split("\\.");
        String[] curparts = currentVersion.split("\\.");

        int i = 0;
        for(String part : parts) {
            if(i >= curparts.length) {
                break;
            }
            int newver = Integer.parseInt(part);
            int curver = Integer.parseInt(curparts[i]);
            if(newver > curver) {
                if(i != 0) {
                    int newverlast = Integer.parseInt(parts[i-1]);
                    int currentverlast = Integer.parseInt(curparts[i-1]);
                    if(newverlast >= currentverlast) {
                        updateAvailable = true;
                        break;
                    }
                } else {
                    updateAvailable = true;
                    break;
                }
            }
            i++;
        }

        if(updateAvailable && !ready) {
            logger.info("An update is available! ("+latestVersion+") Do /"+downloadCommand+" to download it!");
        } else if(!ready) {
            logger.info("You are up to date! ("+latestVersion+")");
        }
        ready = true;
    }

    public boolean isUpdateAvailable() {
        if(!enabled) return false;
        return updateAvailable;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isAlreadyDownloaded() {
        return alreadyDownloaded;
    }

    public boolean downloadUpdate() {
        if(!isEnabled()) {
            logger.warning("Updater is disabled");
            return false;
        }
        if(!isUpdateAvailable()) {
            logger.warning("No update is available!");
            return false;
        }
        if(isAlreadyDownloaded()) {
            logger.warning("The update has already been downloaded!");
            return false;
        }

        List<String> possibleNames = Arrays.asList(
                jarName+"-"+currentVersion,
                jarName.toLowerCase(Locale.ROOT)+"-"+currentVersion,
                jarName.toLowerCase(Locale.ROOT),
                jarName,
                jarName+"-"+currentVersion+" (1)",
                jarName+"-"+currentVersion+" (2)",
                jarName+"-"+currentVersion+" (3)"
        );

        File oldJar = null;

        for(String name : possibleNames) {
            File file = new File(pluginsFolder, name+".jar");
            if(!file.exists()) continue;
            oldJar = file;
            break;
        }

        if(oldJar == null) {
            logger.severe("Could not find the old plugin jar! Make sure it is named like this: "
                    +jarName+"-"+currentVersion+".jar");
            return false;
        }

        try {
            URL url = new URL("https://ajg0702.us/pl/updater/jars/"+jarName+"-"+latestVersion+".jar");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.addRequestProperty("User-Agent", jarName+"/"+currentVersion);
            connection.setInstanceFollowRedirects(true);
            HttpURLConnection.setFollowRedirects(true);


            if(connection.getResponseCode() != 200) {
                throw new IllegalStateException("Response code was "+connection.getResponseCode());
            }

            ReadableByteChannel rbc = Channels.newChannel(connection.getInputStream());
            File out = new File(pluginsFolder, jarName+"-"+latestVersion+".jar");
            logger.info(out.getAbsolutePath());
            FileOutputStream fos = new FileOutputStream(out);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            if(!oldJar.delete()) {
                logger.warning("Unable to delete the old jar! You should delete it yourself before restarting.");
            }
            updateAvailable = false;
            alreadyDownloaded = true;
            return true;

        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public void shutdown() {
        updateExecutor.shutdown();
    }
}
