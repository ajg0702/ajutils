package us.ajg0702.utils.common;

import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.yaml.NodeStyle;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;

@SuppressWarnings("ConfusingArgumentToVarargsMethod")
public class ConfigFile implements ConfigInterface {
	
	File f;
	CommentedConfigurationNode config;
	YamlConfigurationLoader loader;
	
	File pluginFolder;

	Logger logger;
	
	String nfm;

	private final Class<?> getDefaultFrom;

	public Object get(String key) {
		Object r = null;
		try {
			r = config.node(key.split("\\.")).get(Object.class);
		} catch (SerializationException ignored) {}
		if(r == null) {
			logger.severe(nfm.replace("KEY", key));
		}
		return r;
	}
	public Integer getInt(String key) {
		int r = config.node(key.split("\\.")).getInt(-38964298);
		if(r == -38964298) {
			logger.severe(nfm.replace("KEY", key));
		}
		return r;
	}
	public String getString(String key) {
		String r = config.node(key.split("\\.")).getString();
		if(r == null) {
			logger.severe(nfm.replace("KEY", key));
		}
		return r;
	}
	public List<String> getStringList(String key) {
		List<String> r = null;
		try {
			r = config.node(key.split("\\.")).getList(String.class);
		} catch (SerializationException ignored) {}
		if(r == null) {
			logger.severe(nfm.replace("KEY", key));
		}
		return r;
	}
	public boolean getBoolean(String key) {
		return config.node(key.split("\\.")).getBoolean();
	}
	public double getDouble(String key) {
		double d = config.node(key.split("\\.")).getDouble(-38964298);
		if(d == -38964298) {
			logger.severe(nfm.replace("KEY", key));
		}
		return d;
	}

	public boolean hasEntry(String key) {
		return !config.node(key.split("\\.")).virtual();
	}
	
	
	public String getDefaultConfig() throws IOException {
		BufferedReader stream = new BufferedReader(new InputStreamReader(Objects.requireNonNull(getDefaultFrom.getResourceAsStream("/" + fileName))));
		StringBuilder configfile = new StringBuilder();
		String line;
		while((line = stream.readLine()) != null) {
			configfile.append(line).append("\n");
		}
		return configfile.toString();
	}
	
	public String getConfigString() throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(pluginFolder.getPath(), fileName));
		StringBuilder end = new StringBuilder();
		for(String line : lines) {
			end.append(line).append("\n");
		}
		return end.toString();
	}
	String fileName;

	public ConfigFile(File pluginFolder, Logger logger, String fileName) throws ConfigurateException {
		this(pluginFolder, logger, fileName, null);
	}

	@SuppressWarnings("unused")
	public ConfigFile(File pluginFolder, Logger logger, String fileName, Class<?> getDefaultFrom) throws ConfigurateException {
		this.logger = logger;
		this.fileName = fileName;
		this.getDefaultFrom = getDefaultFrom == null ? this.getClass() : getDefaultFrom;
		this.pluginFolder = pluginFolder;
		nfm = "Could not find KEY in "+fileName+"! Try restarting the server, or deleting "+fileName+" and allowing the plugin to re-create it.";
		f = new File(pluginFolder, fileName);
		loader = YamlConfigurationLoader.builder().file(f).nodeStyle(NodeStyle.BLOCK).build();
		if(!f.exists()) {
			if(!Files.exists(pluginFolder.toPath())) {
				try {
					Files.createDirectory(pluginFolder.toPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			try {
				PrintWriter writer = new PrintWriter(pluginFolder+File.separator+fileName, "UTF-8");
				String[] lines = getDefaultConfig().split("\n");
				for(String line : lines) {
					writer.println(line);
					
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//Bukkit.getLogger().info(configfile.toString());
		} else {
			try {
				f = new File(pluginFolder, fileName);
				YamlConfigurationLoader oldLoader = YamlConfigurationLoader.builder().file(f).nodeStyle(NodeStyle.BLOCK).build();
				CommentedConfigurationNode oldConfig = oldLoader.load();
				String newConfig = getDefaultConfig();
				int oldver = oldConfig.node("config-version").getInt(0);
				String strv = newConfig
						.split("config-version: ")[1]
						.split("\n")[0];
				int newver = Integer.parseInt(strv);
				if(oldver < newver) {
					logger.info("Starting config converter!");
					Date date = Calendar.getInstance().getTime();  
	                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
	                String strDate = dateFormat.format(date);  
					duplicateFile(f, new File(pluginFolder, fileName+".old."+strDate));
					String oldConfigString = getConfigString();
					f = new File(pluginFolder, fileName);
					config = loader.load();

					logger.info(oldConfig.childrenMap().size()+" children");
					for(Object keyObject : oldConfig.childrenMap().keySet()) {
						String key = keyObject.toString()+":";
						logger.info("Key: "+key);
						assert key != null;
						String[] keyParts = key.split("\\.");
						String keySec = keyParts[keyParts.length-1];
						logger.info("keySec: "+keySec);
						String newVal = newConfig.split(keySec)[1].split("\n")[0];
						logger.info("newVal before: "+newVal);
						int i = 0;
						for(String l : newConfig.split(keySec)[1].split("\n")) {
							if(i == 0) {
								i++;
								continue;
							}
							i++;
							logger.info("Scanning: "+l);
							if(l.trim().startsWith("-")) {
								newVal += "\n"+l;
							} else {
								break;
							}
						}
						logger.info("newVal after: "+newVal);
						String find = keySec+ newVal;
						CommentedConfigurationNode node = oldConfig.node(key.substring(0, key.length()-1).split("\\."));
						Object nodeObj = node.get(Object.class);
						String nodeString = nodeObj == null ? null : nodeObj.toString();
						if(node.isList()) {
							List<String> list = node.getList(String.class);
						}
						logger.info("nodeString: "+nodeString);
						if(
								!node.isList() && nodeString != null &&
										(
												nodeString.contains(",") || nodeString.contains(".") ||
														nodeString.isEmpty() || nodeString.contains("?")
										) &&
										!nodeString.startsWith("\"") && !nodeString.startsWith("'")

						) {
							nodeString = "\""+nodeString+"\"";
						}
						String replace = keySec + " " + (nodeString != null ? nodeString : "");
						if(!replace.endsWith("[]")) {
							replace = replace.replaceAll("\\[", "\n- \"");
							replace = replace.replaceAll(", ", "\"\n- \"");
							replace = replace.replaceAll("]", "\"");
						}
						if(!keySec.contains("config-version") && !replace.contains("null")) {
							logger.info("Find: "+find +" Replace w: "+replace);
							newConfig = newConfig.replaceAll("\\Q"+ Matcher.quoteReplacement(find)+"\\E", Matcher.quoteReplacement(replace));
						}
					}
					PrintWriter writer = new PrintWriter(f, "UTF-8");
					String[] lines = newConfig.split("\n");
					for(String line : lines) {
						writer.println(line);
					}
					writer.close();
				}
			} catch (IOException e) {
				 logger.severe("Unable to load default config! " + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
			}
		}
		
		f = new File(pluginFolder, fileName);
		config = loader.load();
	}
	public void reload() throws ConfigurateException {
		config = loader.load();
	}

	public CommentedConfigurationNode getConfig() {
		return config;
	}

	public YamlConfigurationLoader getLoader() {
		return loader;
	}

	private static void duplicateFile(File source, File destination) throws IOException {
		try (InputStream is = new FileInputStream(source); OutputStream os = new FileOutputStream(destination)) {
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		}
	}

}
