package us.ajg0702.utils.common;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class UpdateManager {

    private final ScheduledExecutorService updateExecutor = Executors.newScheduledThreadPool(1);

    private final UtilsLogger logger;
    private final String currentVersion;
    private String jarName;
    private final String pluginName;
    private final File pluginsFolder;
    private final String downloadCommand;
    private String updateToken;

    private String latestVersion;

    boolean updateAvailable = false;

    boolean ready = false;

    boolean alreadyDownloaded = false;

    private boolean isPremium = false;

    public UpdateManager(UtilsLogger logger, String currentVersion, String jarName, String pluginName, @Nullable String updateToken, File pluginsFolder, String downloadCommand) {
        this.logger = logger;
        this.jarName = jarName;
        this.pluginName = pluginName;
        this.pluginsFolder = pluginsFolder;
        this.downloadCommand = downloadCommand;
        this.updateToken = updateToken;

        if(currentVersion.contains("-")) {
            this.currentVersion = currentVersion.split("-")[0];
        } else {
            this.currentVersion = currentVersion;
        }

        updateExecutor.scheduleAtFixedRate(() -> {
            try {
                check();
            } catch(Exception e) {
                logger.warn("Unable to check for update: " + e.getMessage());
            }
        }, 2, 30 * 60, TimeUnit.SECONDS);

    }

    public void setUpdateToken(String newToken) {
        updateToken = newToken;
    }

    public void check() throws IOException {
        URL url = new URL("https://plugin-updates.ajg0702.us/plugins/" + pluginName + ".json");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("User-Agent", jarName + "/" + currentVersion);

        if(connection.getResponseCode() == 404) {
            throw new IllegalArgumentException("Plugin name " + pluginName + " does not appear to exist on the server!");
        }
        if(connection.getResponseCode() != 200) {
            throw new IllegalStateException("Response code was " + connection.getResponseCode());
        }

        InputStream inputStream = connection.getInputStream();
        InputStreamReader reader = new InputStreamReader(inputStream);

        JsonObject json = new JsonParser().parse(reader).getAsJsonObject();

        latestVersion = json.get("latest").getAsString();
        isPremium = json.get("premium").getAsBoolean();

        if(latestVersion.contains("-")) {
            latestVersion = latestVersion.split("-")[0];
        }

        if(latestVersion.isEmpty()) {
            throw new IllegalStateException("Latest version is empty!");
        }

        String[] parts = latestVersion.split("\\.");
        String[] currentParts = currentVersion.split("\\.");

        int i = 0;
        for(String part : parts) {
            if(i >= currentParts.length) {
                break;
            }
            int newVersion = Integer.parseInt(part);
            int currentVersion = Integer.parseInt(currentParts[i]);
            if(newVersion > currentVersion) {
                if(i != 0) {
                    int newVersionLast = Integer.parseInt(parts[i-1]);
                    int currentVersionLast = Integer.parseInt(currentParts[i-1]);
                    if(newVersionLast >= currentVersionLast) {
                        updateAvailable = true;
                        break;
                    }
                } else {
                    updateAvailable = true;
                    break;
                }
            }
            i++;
        }

        if(updateAvailable && !ready) {
            logger.info("An update is available! ("+latestVersion+") Run /"+downloadCommand+" to download it!");
        } else if(!ready) {
            logger.info("You are up to date! ("+latestVersion+")");
        }
        ready = true;
    }

    public boolean isUpdateAvailable() {
        return updateAvailable;
    }

    public boolean isAlreadyDownloaded() {
        return alreadyDownloaded;
    }

    public DownloadCompleteStatus downloadUpdate() {
        if(!updateAvailable) {
            try {
                // Maybe its just been a while since we've checked for updates.
                // Check for updates again, then exit if there still isn't an update available.
                check();
            } catch(Exception e) {
                logger.warn("Unable to check for update: " + e.getMessage());
                return DownloadCompleteStatus.ERROR_WHILE_CHECKING;
            }
            if(!updateAvailable) {
                logger.warn("No update is available!");
                return DownloadCompleteStatus.ERROR_NO_UPDATE_AVAILABLE;
            }
        }

        if(isPremium && (updateToken == null || updateToken.isEmpty())) {
            return DownloadCompleteStatus.ERROR_MISSING_UPDATE_TOKEN;
        }

        if(alreadyDownloaded) {
            return DownloadCompleteStatus.ERROR_ALREADY_DOWNLOADED;
        }

        List<String> possibleNames = Arrays.asList(
                jarName+"-"+currentVersion,
                jarName.toLowerCase(Locale.ROOT)+"-"+currentVersion,
                jarName.toLowerCase(Locale.ROOT),
                jarName,
                jarName+"-"+currentVersion+" (1)",
                jarName+"-"+currentVersion+" (2)",
                jarName+"-"+currentVersion+" (3)"
        );

        File oldJar = null;

        for (String possibleName : possibleNames) {
            File file = new File(pluginsFolder, possibleName + ".jar");
            if(!file.exists()) continue;
            oldJar = file;
            break;
        }

        try {
            URL url = new URL("https://plugin-updates.ajg0702.us/jars/" + (isPremium ? "premium" : "free") + "/" + jarName + "-" + latestVersion + ".jar");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.addRequestProperty("User-Agent", jarName + "/" + currentVersion);
            if(isPremium && updateToken != null && !updateToken.isEmpty()) {
                connection.addRequestProperty("Authorization", "Update-Token " + updateToken);
            }

            connection.setInstanceFollowRedirects(true);


            if(connection.getResponseCode() == 401) {
                return DownloadCompleteStatus.ERROR_INVALID_UPDATE_TOKEN;
            }
            if(connection.getResponseCode() != 200) {
                throw new IllegalStateException("Bad response code while downloading: " + connection.getResponseCode() + " " + connection.getResponseMessage());
            }

            ReadableByteChannel readableByteChannel = Channels.newChannel(connection.getInputStream());
            File out = new File(pluginsFolder, jarName+"-"+latestVersion+".jar");
            logger.info("Downloading to " + out.getAbsolutePath());
            FileOutputStream fileOutputStream = new FileOutputStream(out);
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            fileOutputStream.close();
            logger.info("Download complete!");
            updateAvailable = false;
            alreadyDownloaded = true;
            if(!oldJar.delete()) {
                logger.warning("Unable to delete the old jar! You should delete it yourself before restarting.");
                return DownloadCompleteStatus.WARNING_COULD_NOT_DELETE_OLD_JAR;
            }
            return DownloadCompleteStatus.SUCCESS;

        } catch(Exception e) {
            logger.warn("Error while downloading jar: " + e.getMessage());
            return DownloadCompleteStatus.ERROR_WHILE_DOWNLOADING;
        }
    }

    public enum DownloadCompleteStatus {
        SUCCESS,
        /** Returned if the download succeeded, but the old jar could not be deleted. Make sure to say that it should be deleted manually before restarting */
        WARNING_COULD_NOT_DELETE_OLD_JAR,
        /** Returned if there is no update available */
        ERROR_NO_UPDATE_AVAILABLE,
        /** Returned if an error occurred while re-checking for an update. */
        ERROR_WHILE_CHECKING,
        /** Returned if the update has already been downloaded */
        ERROR_ALREADY_DOWNLOADED,
        /** Returned if the jar file could not be found (e.g. if the jar is named something weird) */
        ERROR_JAR_NOT_FOUND,
        /** Returned if an error occurred while downloading the jar */
        ERROR_WHILE_DOWNLOADING,
        /** Returned if the plugin is premium, but we're missing the update token */
        ERROR_MISSING_UPDATE_TOKEN,
        /** Returned if the update token was rejected by the update server. Suggest re-generating the token */
        ERROR_INVALID_UPDATE_TOKEN,
    }
}
