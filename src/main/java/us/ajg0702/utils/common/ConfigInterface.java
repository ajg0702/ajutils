package us.ajg0702.utils.common;

import org.spongepowered.configurate.ConfigurateException;

import java.util.List;

public interface ConfigInterface {
    Object get(String key);
    Integer getInt(String key);
    String getString(String key);
    List<String> getStringList(String key);
    boolean getBoolean(String key);
    double getDouble(String key);
    public void reload() throws ConfigurateException;
}
