package us.ajg0702.utils.common;

import com.google.common.collect.ImmutableMap;
import me.clip.placeholderapi.PlaceholderAPI;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentBuilder;
import net.kyori.adventure.text.ComponentIteratorType;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.bungeecord.BungeeComponentSerializer;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.yaml.NodeStyle;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;

@SuppressWarnings({"unused", "ConfusingArgumentToVarargsMethod"})
public class Messages {
	
	File msgFile;
	File pluginFolder;
	
	YamlConfigurationLoader loader;
	ConfigFile config;
	
	CommentedConfigurationNode msgs;
	
	Logger logger;
	
	Map<String, Object> defaultValues;
	Map<String, String> moving;

	private final Random random = new Random();

	
	public Messages(File pluginFolder, Logger logger, Map<String, Object> defaultValues, Map<String, String> moving) {
		this.logger = logger;
		this.pluginFolder = pluginFolder;
		this.defaultValues = defaultValues;
		this.moving = moving;
		loadMessagesFile(defaultValues, moving);
	}

	public Messages(File pluginFolder, Logger logger, Map<String, Object> defaultValues) {
		this.logger = logger;
		this.pluginFolder = pluginFolder;
		this.defaultValues = defaultValues;
		this.moving = new HashMap<>();
		loadMessagesFile(defaultValues, moving);
	}
	
	private void loadMessagesFile(Map<String, Object> defaultMessages, Map<String, String> moving) {
		msgFile = new File(pluginFolder, "messages.yml");
		loader = YamlConfigurationLoader.builder().file(msgFile).nodeStyle(NodeStyle.BLOCK).build();
		try {
			msgs = loader.load();
		} catch (IOException e) {
			logger.severe("Unable to load messages file:");
			e.printStackTrace();
			return;
		}
		for(String rkey : moving.keySet()) {
			String[] key = rkey.split("\\.");
			if(!msgs.hasChild(key)) continue;
			ConfigurationNode node = msgs.node(key);
			if(node.isList()) {
				try {
					msgs.node(moving.get(rkey)).set(msgs.node(key).getList(String.class));
				} catch (SerializationException e) {
					logger.log(Level.SEVERE, "Unable to move message:", e);
				}
			}
		}
		for(String key : defaultMessages.keySet()) {
			if(!msgs.hasChild(key.split("\\."))) {
				try {
					msgs.node(key.split("\\.")).node().set(defaultMessages.get(key));
				} catch (SerializationException e) {
					logger.log(Level.SEVERE, "Unable to set message in file:", e);
				}
			}
		}
		try {
			loader.save(msgs);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Unable to save messages file:", e);
		}
	}

	/**
	 * Get a string message. Deprecated: Use getBC instead. (or getString if you really only need a string)
	 * @param key the key
	 * @return the return message
	 */
	@Deprecated
	public String get(String key) {
		String msg = msgs.node(key.split("\\.")).getString("&cMessage '"+key+"' does not exist!");
		msg = color(msg);
		return msg;
	}

	public String getRawString(String key, String... placeholders) {
		String msg = msgs.node(key.split("\\.")).getString("&cMessage '"+key+"' does not exist!");
		for(String sr : placeholders) {
			String placeholder = sr.split(":")[0];
			String value = sr.replaceFirst(Matcher.quoteReplacement(placeholder+":"), "");
			msg = msg.replaceAll("\\{"+Matcher.quoteReplacement(placeholder)+"}", value);
		}
		return msg;
	}

	public String getString(String key, String... placeholders) {
		String msg = msgs.node(key.split("\\.")).getString("&cMessage '"+key+"' does not exist!");
		for(String sr : placeholders) {
			String placeholder = sr.split(":")[0];
			String value = sr.replaceFirst(Matcher.quoteReplacement(placeholder+":"), "");
			msg = msg.replaceAll("\\{"+Matcher.quoteReplacement(placeholder)+"}", value);
		}
		msg = color(msg);
		return msg;
	}

	public List<String> getStringList(String key, String... placeholders) {
		List<String> msgs;
		try {
			if(hasMessage(key)) {
				msgs = this.msgs.node(key.split("\\.")).getList(String.class);
				if(msgs != null) {
					msgs = new ArrayList<>(msgs);
				}
			} else {
				msgs = new ArrayList<>();
				msgs.add("&cMessage '" + key + "' does not exist!");
			}
		} catch (SerializationException e) {
			logger.log(Level.WARNING, "List '"+key+"' must be all strings!", e);
			return new ArrayList<>();
		}
		for (int i = 0; i < msgs.size(); i++) {
			String msg = color(msgs.get(i));
			for(String sr : placeholders) {
				String placeholder = sr.split(":")[0];
				String value = sr.replaceFirst(Matcher.quoteReplacement(placeholder+":"), "");
				msg = msg.replaceAll("\\{"+Matcher.quoteReplacement(placeholder)+"}", value);
			}
			msgs.set(i, msg);
		}
		return msgs;
	}

	private final LegacyComponentSerializer legacyComponentSerializer = LegacyComponentSerializer.legacySection();

	/**
	 * this is getStringList, but also parsed for minimessage then serialized to legacy
	 */
	public List<String> getMMStringList(String key, String... placeholders) {
		List<String> msgs = getStringList(key, placeholders);
		for (int i = 0; i < msgs.size(); i++) {
			String msg = msgs.get(i);
			Component component = getMiniMessage().deserialize(msg);
			msg = legacyComponentSerializer.serialize(component);
			msgs.set(i, msg);
		}
		return msgs;
	}


	public String getMMString(String key, String... placeholders) {
		return getMMString(key, false, placeholders);
	}
	public String getMMString(String key, boolean preserveColor, String... placeholders) {
		String msg = getString(key, placeholders);
		if(preserveColor) msg += "a";
		Component component = getMiniMessage().deserialize(msg);
		msg = legacyComponentSerializer.serialize(component);
		if(preserveColor) msg = msg.substring(0, msg.length()-1);
		return msg;
	}

	public List<Component> getComponentList(String key, String... placeholders) {
		List<Component> r = new ArrayList<>();
		List<String> msgs = getStringList(key, placeholders);
		for(String s : msgs) {
			r.add(toComponent(s));
		}
		return r;
	}

	public boolean hasMessage(String key) {
		ConfigurationNode node = msgs.node(key.split("\\."));
		return !node.virtual() &&
				!node.isNull();
	}

	public Component getRandomFromList(String key, String... placeholders) {
		List<String> msgs = getStringList(key, placeholders);
		String msg = msgs.get(random.nextInt(msgs.size()-1));
		return toComponent(msg);
	}


	public boolean isEmpty(String key) {
		return getString(key).length() == 0;
	}

	/**
	 * Get a BaseComponent[] from the messages file
	 * @param key The message key
	 * @param placeholders any placeholders to parse
	 * @return A BaseComponent[] with the requested info.
	 */
	public net.md_5.bungee.api.chat.BaseComponent[] getBC(String key, String... placeholders) {
		return getBC(getComponent(key, placeholders));
	}
	public net.md_5.bungee.api.chat.BaseComponent[] getBC(Component component) {
		return BungeeComponentSerializer.get().serialize(component);
	}


	public Component getComponent(String key, String... placeholders) {
		String m = getString(key, placeholders);
		return getMiniMessage().deserialize(m);
	}

	private MiniMessage miniMessage;
	public MiniMessage getMiniMessage() {
		if(miniMessage == null) {
			miniMessage = MiniMessage.miniMessage();
		}
		return miniMessage;
	}

	public Component toComponent(String message) {
		return getMiniMessage().deserialize(color(message));
	}

	private static final ImmutableMap<String, String> colorReplacements = new ImmutableMap.Builder<String, String>()
			.put("0", "<black>")
			.put("1", "<dark_blue>")
			.put("2", "<dark_green>")
			.put("3", "<dark_aqua>")
			.put("4", "<dark_red>")
			.put("5", "<dark_purple>")
			.put("6", "<gold>")
			.put("7", "<gray>")
			.put("8", "<dark_gray>")
			.put("9", "<blue>")
			.put("a", "<green>")
			.put("b", "<aqua>")
			.put("c", "<red>")
			.put("d", "<light_purple>")
			.put("e", "<yellow>")
			.put("f", "<white>")
			.put("k", "<magic>")
			.put("l", "<bold>")
			.put("m", "<strikethrough>")
			.put("n", "<underlined>")
			.put("o", "<italic>")
			.put("r", "<reset>")
			.build();

	public static String color(String msg) {
		for(Map.Entry<String, String> entry : colorReplacements.entrySet()) {
			String legacy = entry.getKey();
			String mini = entry.getValue();
			msg = msg.replaceAll(Matcher.quoteReplacement("&"+legacy), Matcher.quoteReplacement(mini));
			msg = msg.replaceAll(Matcher.quoteReplacement(((char)0x00b7)+legacy), Matcher.quoteReplacement(mini));
		}
		return msg;
	}
	
	public void reload() {
		loadMessagesFile(defaultValues, moving);
	}

	public CommentedConfigurationNode getRootNode() {
		return msgs;
	}
	public void save() {
		try {
			loader.save(msgs);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Unable to save messages file:", e);
		}
	}

	public static Map<String, Object> makeDefaults(Object... args) {
		Map<String, Object> defaults = new LinkedHashMap<>();

		for (int i = 0; i < args.length; i+=2) {
			if(!(args[i] instanceof String)) throw new IllegalArgumentException("Key must be string! It was "+args[i].toString());
			if(i+1 >= args.length) throw new IllegalArgumentException("Odd arguments supplied! (must have a key and a value)");
			defaults.put((String) args[i], args[i+1]);
		}
		return defaults;
	}

	private static Boolean hasPapi;
	public static boolean hasPlaceholderAPI() {
		if(hasPapi == null) {
			try {
				Class.forName("me.clip.placeholderapi.PlaceholderAPI");
				hasPapi = true;
			} catch (ClassNotFoundException e) {
				hasPapi = false;
			}
		}
		return hasPapi;
	}

	public static Component setPlaceholders(Player player, Component component) {
		if(!hasPlaceholderAPI()) return component;

		return recurseSetPlaceholders(player, component);
	}

	private static Component recurseSetPlaceholders(Player player, Component component) {
		if(!(component instanceof TextComponent)) return component;
		TextComponent textComponent = (TextComponent) component;
		Component newComponent = textComponent.content(PlaceholderAPI.setPlaceholders(player, textComponent.content()));

		if(newComponent.children().size() > 0)  {
			List<Component> newChildren = new ArrayList<>();
			for (Component child : newComponent.children()) {
				newChildren.add(recurseSetPlaceholders(player, child));
			}
			newComponent = newComponent.children(newChildren);
		}
		return newComponent;
	}
}
