package us.ajg0702.utils.common;

import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.loader.HeaderMode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.yaml.NodeStyle;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple config that doesnt support comments
 */
public class SimpleConfig implements WritableConfigInterface {

    private final YamlConfigurationLoader loader;
    private CommentedConfigurationNode parentNode;
    private final Logger logger;

    public SimpleConfig(File folder, String fileName, Logger logger) throws ConfigurateException {
        this(folder, fileName, logger, null);
    }
    public SimpleConfig(File folder, String fileName, Logger logger, String header) throws ConfigurateException {
        this.logger = logger;

        File file = new File(folder, fileName);

        YamlConfigurationLoader.Builder loaderBuilder = YamlConfigurationLoader.builder()
                .headerMode(HeaderMode.PRESET)
                .file(file)
                .nodeStyle(NodeStyle.BLOCK);
        if(header != null) loaderBuilder.defaultOptions(loaderBuilder.defaultOptions().header(header));

        loader = loaderBuilder.build();

        if(!file.exists()) {
            if(!Files.exists(folder.toPath())) {
                try {
                    Files.createDirectory(folder.toPath());
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Unable to folder for "+fileName+":", e);
                }
            }

            try {
                InputStream defaultStream = getClass().getResourceAsStream("/"+fileName);
                if(defaultStream != null) {
                    Files.copy(defaultStream, file.toPath());
                }
            } catch (IOException e) {
                logger.log(Level.WARNING, "Unable to create default file for "+fileName+":", e);
            }
        }

        parentNode = loader.load();
    }

    /**
     * Gets the node with a certain path
     * @param path the path
     * @return the node
     */
    public CommentedConfigurationNode getNode(String path) {
        return parentNode.node(path(path));
    }

    /**
     * Gets the base node
     * @return the base node
     */
    public CommentedConfigurationNode getNode() {
        return parentNode;
    }
    
    private Object[] path(String s) {
        return s.split("\\.");
    }

    @Override
    public Object get(String key) {
        try {
            return parentNode.node(path(key)).get(Object.class);
        } catch (SerializationException e) {
            return null;
        }
    }

    @Override
    public Integer getInt(String key) {
        return parentNode.node(path(key)).getInt(-1);
    }

    @Override
    public String getString(String key) {
        return parentNode.node(path(key)).getString();
    }

    @Override
    public List<String> getStringList(String key) {
        try {
            return parentNode.node(path(key)).getList(String.class);
        } catch (SerializationException e) {
            throw new IllegalArgumentException(key+" is not a list! "+e.getMessage());
        }
    }

    @Override
    public boolean getBoolean(String key) {
        return parentNode.node(path(key)).getBoolean();
    }

    @Override
    public double getDouble(String key) {
        return parentNode.node(path(key)).getDouble();
    }

    @Override
    public void reload() throws ConfigurateException {
        parentNode = loader.load();
    }

    @Override
    public void set(String key, Object value) throws SerializationException {
        parentNode.node(path(key)).set(value);
    }

    @Override
    public void setInt(String key, int value) throws SerializationException {
        parentNode.node(path(key)).set(value);
    }

    @Override
    public void setString(String key, String value) throws SerializationException {
        parentNode.node(path(key)).set(value);
    }

    @Override
    public void setStringList(String key, List<String> value) throws SerializationException {
        parentNode.node(path(key)).set(value);
    }

    @Override
    public void setBoolean(String key, boolean value) throws SerializationException {
        parentNode.node(path(key)).set(value);
    }

    @Override
    public void setDouble(String key, double value) throws SerializationException {
        parentNode.node(path(key)).set(value);
    }

    @Override
    public void save() throws IOException {
        loader.save(parentNode);
    }
}
