package us.ajg0702.utils.common;

import org.jetbrains.annotations.NotNull;

import java.util.logging.LogRecord;
import java.util.logging.Logger;

@SuppressWarnings("unused")
public class VoidLogger extends Logger {

    public VoidLogger() {
        super("void", null);
    }

    @Override
    public void log(@NotNull LogRecord logRecord) {}
}
