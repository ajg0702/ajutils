package us.ajg0702.utils.common;

import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.serialize.SerializationException;

import java.io.IOException;
import java.util.List;

public interface WritableConfigInterface extends ConfigInterface{
    void set(String key, Object value) throws SerializationException;
    void setInt(String key, int value) throws SerializationException;
    void setString(String key, String value) throws SerializationException;
    void setStringList(String key, List<String> value) throws SerializationException;
    void setBoolean(String key, boolean value) throws SerializationException;
    void setDouble(String key, double value) throws SerializationException;
    void save() throws IOException;
}
