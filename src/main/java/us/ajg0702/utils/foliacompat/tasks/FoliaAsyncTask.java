package us.ajg0702.utils.foliacompat.tasks;

import us.ajg0702.utils.foliacompat.Task;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class FoliaAsyncTask implements Task {

    private final Object handle;

    public FoliaAsyncTask(Object handle) {
        this.handle = handle;
    }

    @Override
    public void cancel() {
        if(isCancelled()) return;
        try {
            Method method = handle.getClass().getMethod("cancel");
            method.setAccessible(true);
            method.invoke(handle);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isCancelled() {
        try {
            return (boolean) handle.getClass().getMethod("isCancelled").invoke(handle);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
}
