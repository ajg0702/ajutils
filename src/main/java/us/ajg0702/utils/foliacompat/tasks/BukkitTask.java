package us.ajg0702.utils.foliacompat.tasks;

import us.ajg0702.utils.foliacompat.Task;
import us.ajg0702.utils.spigot.VersionSupport;

public class BukkitTask implements Task {
    private final org.bukkit.scheduler.BukkitTask handle;

    /** On <1.12, isCancelled doesn't exist, so we need to track it ourselves and hope nothing goes around this wrapper */
    private boolean cancelledHere = false;

    public BukkitTask(org.bukkit.scheduler.BukkitTask handle) {
        this.handle = handle;
    }

    @Override
    public void cancel() {
        if(isCancelled()) return;
        handle.cancel();
        cancelledHere = true;
    }

    @Override
    public boolean isCancelled() {
        if(VersionSupport.getMinorVersion() < 12) {
            return cancelledHere;
        }
        return handle.isCancelled();
    }
}
