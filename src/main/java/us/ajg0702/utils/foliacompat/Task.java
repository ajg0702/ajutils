package us.ajg0702.utils.foliacompat;

public interface Task {
    void cancel();
    boolean isCancelled();
}
