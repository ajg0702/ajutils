package us.ajg0702.utils.foliacompat;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitWorker;
import us.ajg0702.utils.foliacompat.tasks.BukkitTask;
import us.ajg0702.utils.foliacompat.tasks.FoliaAsyncTask;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class CompatScheduler {
    public final Plugin plugin;

    private static Boolean isFolia;

    public CompatScheduler(Plugin plugin) {
        this.plugin = plugin;
    }

    public void runSync(Location location, Runnable task) {
        if(isFolia()) {
            Object regionScheduler = getRegionScheduler();
            try {
                regionScheduler.getClass().getMethod("execute", Plugin.class, Location.class, Runnable.class)
                        .invoke(regionScheduler, plugin, location, task);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        } else {
            Bukkit.getScheduler().runTask(plugin, task);
        }
    }

    public void runSync(Entity entity, Runnable task) {
        if(isFolia()) {
            try {
                Object entityScheduler = entity.getClass().getMethod("getScheduler").invoke(entity);
                entityScheduler.getClass().getMethod("run", Plugin.class, Consumer.class, Runnable.class)
                        .invoke(entityScheduler, plugin, (Consumer<?>)(a) -> task.run(), null);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        } else {
            Bukkit.getScheduler().runTask(plugin, task);
        }
    }

    public void runTaskAsynchronously(Runnable task) {
        if(isFolia()) {
            Object asyncScheduler = getAsyncScheduler();
            try {
                asyncScheduler.getClass().getMethod("runNow", Plugin.class, Consumer.class)
                        .invoke(asyncScheduler, plugin, (Consumer<?>)(a) -> task.run());
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        } else {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, task);
        }
    }

    public Task runTaskLaterAsynchronously(Runnable task, long delay) {
        if(isFolia()) {
            Object asyncScheduler = getAsyncScheduler();
            try {
                return new FoliaAsyncTask(
                        asyncScheduler.getClass().getMethod("runDelayed", Plugin.class, Consumer.class, long.class, TimeUnit.class)
                                .invoke(asyncScheduler, plugin, (Consumer<?>)(a) -> task.run(), delay * 50, TimeUnit.MILLISECONDS)
                );
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        } else {
            return new BukkitTask(
                    Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, task, delay)
            );
        }
    }

    public List<BukkitWorker> getActiveWorkers() {
        if(isFolia()) {
            return Collections.emptyList();
        } else {
            return Bukkit.getScheduler().getActiveWorkers();
        }
    }

    public Task runTaskTimerAsynchronously(Runnable task, long delay, long period) {
        if(isFolia()) {
            Object asyncScheduler = getAsyncScheduler();
            try {
                return new FoliaAsyncTask(
                        asyncScheduler.getClass().getMethod("runAtFixedRate", Plugin.class, Consumer.class, long.class, long.class, TimeUnit.class)
                                .invoke(asyncScheduler, plugin, (Consumer<?>)(a) -> task.run(), delay * 50, period * 50, TimeUnit.MILLISECONDS)
                );
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                plugin.getLogger().warning(Arrays.toString(asyncScheduler.getClass().getMethods()));
                throw new RuntimeException(e);
            }
        } else {
            return new BukkitTask(
                    Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, task, delay, period)
            );
        }
    }

    public void cancelTasks() {
        if(isFolia()) {
            Object asyncScheduler = getAsyncScheduler();
            try {
                asyncScheduler.getClass().getMethod("cancelTasks", Plugin.class)
                        .invoke(asyncScheduler, plugin);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        } else {
            Bukkit.getScheduler().cancelTasks(plugin);
        }
    }

    public static Object getRegionScheduler() {
        if(!isFolia()) throw new IllegalStateException("Cannot get region scheduler from non-folia!");
        try {
            return Bukkit.class.getDeclaredMethod("getRegionScheduler").invoke(null);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
    public static Object getAsyncScheduler() {
        if(!isFolia()) throw new IllegalStateException("Cannot get region scheduler from non-folia!");
        try {
            return Bukkit.class.getDeclaredMethod("getAsyncScheduler").invoke(null);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isFolia() {
        if(isFolia == null) {
            try {
                Class.forName("io.papermc.paper.threadedregions.RegionizedServer");
                isFolia = true;
            } catch (ClassNotFoundException e) {
                isFolia = false;
            }
        }
        return isFolia;
    }
}
