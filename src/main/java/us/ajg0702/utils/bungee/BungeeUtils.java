package us.ajg0702.utils.bungee;

import java.util.Collection;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeUtils {
	public static void sendCustomData(ProxiedPlayer player, String channel, String... data) {
	    Collection<ProxiedPlayer> networkPlayers = ProxyServer.getInstance().getPlayers();
	    // perform a check to see if globally are no players
	    if ( networkPlayers == null || networkPlayers.isEmpty()) return;

	    ByteArrayDataOutput out = ByteStreams.newDataOutput();
	    out.writeUTF( channel );
	    out.writeUTF(player.getName());
	    for(String s : data) {
	    	out.writeUTF( s );
	    }
	 
	    // we send the data to the server
	    // using ServerInfo the packet is being queued if there are no players in the server
	    // using only the server to send data the packet will be lost if no players are in it
	    player.getServer().sendData( "ajqueue:tospigot", out.toByteArray() );
	}
}
