package us.ajg0702.utils.bungee;


import java.util.List;

import net.md_5.bungee.api.plugin.Plugin;
import org.spongepowered.configurate.ConfigurateException;
import us.ajg0702.utils.common.ConfigFile;

@SuppressWarnings("unused")
public class BungeeConfig {

	ConfigFile cf;

	public BungeeConfig(Plugin pl) throws ConfigurateException {
		cf = new ConfigFile(pl.getDataFolder(), pl.getLogger(), "config.yml");
	}

	public Object get(String key) {
		return cf.get(key);
	}
	public Integer getInt(String key) {
		return cf.getInt(key);
	}
	public String getString(String key) {
		return cf.getString(key);
	}
	public List<String> getStringList(String key) {
		return cf.getStringList(key);
	}
	public boolean getBoolean(String key) {
		return cf.getBoolean(key);
	}

	public void reload() throws ConfigurateException {
		cf.reload();
	}
}
