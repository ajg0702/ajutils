package us.ajg0702.utils.bungee;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.bungeecord.BungeeComponentSerializer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;

public class BungeeMessages {
	static BungeeMessages INSTANCE = null;
	public static BungeeMessages getInstance() {
		return INSTANCE;
	}
	public static BungeeMessages getInstance(Plugin pl, LinkedHashMap<String, String> defaultValues) {
		if(INSTANCE == null) {
			INSTANCE = new BungeeMessages(pl, defaultValues);
		}
		return INSTANCE;
	}
	
	Plugin pl;
	ConfigurationProvider cv = ConfigurationProvider.getProvider(YamlConfiguration.class);
	Configuration msgs;
	File msgFile;
	LinkedHashMap<String, String> defaultValues;
	private BungeeMessages(Plugin pl, LinkedHashMap<String, String> defaultValues) {
		this.pl = pl;
		this.defaultValues = defaultValues;
		loadMessagesFile(defaultValues);
	}
	
	private void loadMessagesFile(LinkedHashMap<String, String> d) {
		msgFile = new File(pl.getDataFolder(), "messages.yml");
		if(!msgFile.exists()) {
			try {
				pl.getDataFolder().mkdirs();
				msgFile.createNewFile();
			} catch (IOException e) {
				pl.getLogger().severe("Unable to create messages file:");
				e.printStackTrace();
			}
		}
		try {
			msgs = cv.load(msgFile);
		} catch (IOException e) {
			pl.getLogger().severe("Unable to load messages file:");
			e.printStackTrace();
			return;
		}
		
		for(String k : d.keySet()) {
			//pl.getLogger().info("Checking "+k);
			if(!msgs.contains(k)) {
				msgs.set(k, d.get(k));
			}
		}
		try {
			cv.save(msgs, msgFile);
		} catch (IOException e) {
			pl.getLogger().severe("Unable to save messages file:");
			e.printStackTrace();
		}
	}

	/**
	 * Get a string message. Deprecated: Use getBC instead. (or getString if you really only need a string)
	 * @param key the key
	 * @return the return message
	 */
	@Deprecated
	public String get(String key) {
		String msg = msgs.get(key, "&cMessage '"+key+"' does not exist!");
		msg = color(msg);
		return msg;
	}

	public String getString(String key, String... placeholders) {
		String msg = msgs.get(key, "&cMessage '"+key+"' does not exist!");
		msg = color(msg);
		for(String sr : placeholders) {
			String placeholder = sr.split(":")[0];
			String value = sr.replaceFirst(Matcher.quoteReplacement(placeholder+":"), "");
			msg = msg.replaceAll("\\{"+Matcher.quoteReplacement(placeholder)+"}", value);
		}
		return msg;
	}

	/**
	 * Get a BaseComponent[] from the messages file
	 * @param key The message key
	 * @param placeholders any placeholders to parse
	 * @return A BaseComponent[] with the requested info.
	 */
	public BaseComponent[] getBC(String key, String... placeholders) {
		return getBC(getComponent(key, placeholders));
	}
	public BaseComponent[] getBC(Component component) {
		return BungeeComponentSerializer.get().serialize(component);
	}


	public Component getComponent(String key, String... placeholders) {
		String m = getString(key, placeholders);
		return MiniMessage.miniMessage().deserialize(m);
	}

	public String color(String msg) {
		
		return net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', msg).replaceAll(Matcher.quoteReplacement("\\n"), "\n");
	}
	
	public void reload() {
		loadMessagesFile(defaultValues);
	}
}
