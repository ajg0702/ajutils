package us.ajg0702.utils.spigot;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class APEvents implements Listener {
    AManager man;
    protected APEvents(AManager m) {
        man = m;
    }
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        man.createPlayer(e.getPlayer());
    }
    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        man.removePlayer(e.getPlayer());
    }
}
