package us.ajg0702.utils.spigot;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;

public class APlayer {
	
	private Player player;
	
	protected APlayer(Player p) {
		this.player = p;
	}
	
	public void sendMessage(String key, String... placeholders) {
		BaseComponent[] s = Messages.getInstance().getComponent(key, placeholders);
		if(s.length == 0) return;
		player.spigot().sendMessage(s);
	}
	public void sendRawMessage(String msg) {
		player.sendMessage(msg);
	}
	
	public Player getPlayer() {
		return player;
	}
}
