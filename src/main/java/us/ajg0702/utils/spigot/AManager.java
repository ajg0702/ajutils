package us.ajg0702.utils.spigot;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class AManager {
    private static AManager instance = null;

    public AManager getInstance() {
        return instance;
    }

    public AManager getInstance(JavaPlugin pl) {
        if (instance == null) {
            instance = new AManager(pl);
        }
        return instance;
    }

    private AManager(JavaPlugin pl) {
        Bukkit.getPluginManager().registerEvents(new APEvents(this), pl);
    }


    HashMap<Player, APlayer> players = new HashMap<>();

    protected void createPlayer(Player ply) {
        players.put(ply, new APlayer(ply));
    }
    protected void removePlayer(Player ply) {
        players.remove(ply);
    }

    public APlayer getPlayer(Player ply) {
        return players.get(ply);
    }

}
