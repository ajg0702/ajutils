package us.ajg0702.utils.spigot;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.jetbrains.annotations.Nullable;

public class LocUtils {
	
	public static String locToString(@Nullable Location l) {
		if(l == null) return null;
		if(l.getWorld() == null) throw new IllegalArgumentException("World cannot be null");
		return l.getWorld().getName()+","
			 + l.getX()+","
			 + l.getY()+","
			 + l.getZ()+","
			 + l.getYaw()+","
			 + l.getPitch()+",";
	}
	public static Location stringToLoc(@Nullable String s) {
		if(s == null) return null;
		String[] parts = s.split(",");
		return new Location(
				Bukkit.getWorld(parts[0]),
				dv(parts[1]),
				dv(parts[2]),
				dv(parts[3]),
				fv(parts[4]),
				fv(parts[5]));
	}

	public static Location center(Location loc) {
		Location location = loc.clone();
		location.setX(location.getBlockX()+0.5);
		location.setY(location.getBlockY()+0.5);
		location.setZ(location.getBlockZ()+0.5);
		return location;
	}

	public static class NoWorld {
		public static String locToString(@Nullable Location l) {
			if(l == null) return null;
			return l.getX()+","
					+ l.getY()+","
					+ l.getZ()+","
					+ l.getYaw()+","
					+ l.getPitch()+",";
		}
		public static Location stringToLoc(World world, @Nullable String s) {
			if(s == null) return null;
			String[] parts = s.split(",");
			return new Location(
					world,
					dv(parts[0]),
					dv(parts[1]),
					dv(parts[2]),
					fv(parts[3]),
					fv(parts[4]));
		}
	}
	
	private static double dv(String s) {
		return Double.valueOf(s);
	}
	private static float fv(String s) {
		return Float.valueOf(s);
	}
}
