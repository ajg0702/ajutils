package us.ajg0702.utils.spigot;

import org.bukkit.plugin.java.JavaPlugin;
import org.spongepowered.configurate.ConfigurateException;

@SuppressWarnings("unused")
public class SpigotConfig extends us.ajg0702.utils.common.Config {
	public SpigotConfig(JavaPlugin pl) throws ConfigurateException {
		super(pl.getDataFolder(), pl.getLogger());
	}
}
