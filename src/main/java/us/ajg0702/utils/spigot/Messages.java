package us.ajg0702.utils.spigot;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.regex.Matcher;

import org.bukkit.plugin.java.JavaPlugin;

import net.kyori.adventure.platform.bukkit.BukkitAudiences;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.bungeecord.BungeeComponentSerializer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;

import org.bukkit.configuration.file.YamlConfiguration;

@Deprecated
public class Messages {
	static Messages INSTANCE = null;
	public static Messages getInstance() {
		return INSTANCE;
	}
	public static Messages getInstance(JavaPlugin pl, LinkedHashMap<String, String> defaultValues) {
		if(INSTANCE == null) {
			INSTANCE = new Messages(pl, defaultValues);
		}
		return INSTANCE;
	}
	
	JavaPlugin pl;
	YamlConfiguration msgs;
	File msgFile;
	private BukkitAudiences adventure;
	LinkedHashMap<String, String> defaultValues;
	private Messages(JavaPlugin pl, LinkedHashMap<String, String> defaultValues) {
		this.pl = pl;
		this.defaultValues = defaultValues;
		loadMessagesFile(defaultValues);
		
		adventure = BukkitAudiences.create(pl);
		
		
	}
	
	private void loadMessagesFile(LinkedHashMap<String, String> d) {
		msgFile = new File(pl.getDataFolder(), "messages.yml");
		if(!msgFile.exists()) {
			try {
				pl.getDataFolder().mkdirs();
				msgFile.createNewFile();
			} catch (IOException e) {
				pl.getLogger().severe("Unable to create messages file:");
				e.printStackTrace();
			}
		}
		msgs = YamlConfiguration.loadConfiguration(msgFile);
		
		for(String k : d.keySet()) {
			//pl.getLogger().info("Checking "+k);
			if(!msgs.contains(k)) {
				msgs.set(k, d.get(k));
			}
		}
		try {
			msgs.save(msgFile);
		} catch (IOException e) {
			pl.getLogger().severe("Unable to save messages file:");
			e.printStackTrace();
		}
	}
	
	
	public String get(String key) {
		String msg = msgs.getString(key, "&cMessage '"+key+"' does not exist!");
		msg = color(msg);
		return msg;
	}
	public String get(String key, String... placeholders) {
		String msg = get(key);
		for(String sr : placeholders) {
			String placeholder = sr.split(":")[0];
			String value = sr.replaceFirst(Matcher.quoteReplacement(placeholder+":"), "");
			msg = msg.replaceAll("\\{"+Matcher.quoteReplacement(placeholder)+"\\}", value);
		}
		return msg;
	}
	
	
	public BaseComponent[] getComponent(String key, String... placeholders) {
		String msg = msgs.getString(key, "&cMessage '"+key+"' does not exist!");
		msg = color(msg);
		for(String sr : placeholders) {
			String placeholder = sr.split(":")[0];
			String value = sr.replaceFirst(Matcher.quoteReplacement(placeholder+":"), "");
			msg = msg.replaceAll("\\{"+Matcher.quoteReplacement(placeholder)+"\\}", value);
		}
		return BungeeComponentSerializer.get().serialize(MiniMessage.miniMessage().deserialize(msg));
	}
	
	
	public String color(String msg) {
		
		return net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', msg).replaceAll(Matcher.quoteReplacement("\\n"), "\n");
	}
	
	public void reload() {
		loadMessagesFile(defaultValues);
	}
}
